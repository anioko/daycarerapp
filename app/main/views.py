from flask import abort, flash, redirect, render_template, url_for, request
from ..models import *
from .forms import *
from flask_login import current_user, login_required
from . import main
from .. import *
from ..models import Bookings
from datetime import date, datetime
from jinja2 import Environment


ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

import os
from flask import Flask, render_template, request
import stripe

stripe_keys = {
    'secret_key': 'sk_test_fPHKYTWdK5ZhuxQ3me9TOndf',
    'publishable_key': 'pk_test_EUm15FduMZOImjeIy7dsKjdE'
}


stripe.api_key = stripe_keys['secret_key']

#app = Flask(__name__)
childminder_id=0

@main.route('/checkout/<int:amount>')
def checkout(amount):
    booking_form = BookingForm()
    return render_template('main/thankyou.html',amount=amount, form=booking_form , key=stripe_keys['publishable_key'])

@main.route('/charge', methods=['POST'])
def charge():

  customer = stripe.Customer.create(
    email='ahsanaliaslam@gmail.com',
    card=request.form['stripeToken']
    )
  amount = request.form['amount']
  charge = stripe.Charge.create(
    customer=customer.id,
    amount=amount,
    currency='usd',
    description='Flask Charge'
    )

  changetodollar = float(float(amount) / 100)
  global childminder_id

  payment_detail = Payments(
     carer_id=childminder_id,
      amount=changetodollar,
      parent_id=current_user.id,
  )

  db.session.add(payment_detail)
  db.session.commit()
  flash("Photo saved.")
  return redirect(url_for('main.index'))
  return render_template('main/dashboard.html', user=current_user)

#if __name__ == '__main__':
#  app.run(debug=True)

@main.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    form = PhotoForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            image_filename = images.save(request.files['photo'])
            image_url = images.url(image_filename)
            picture_photo = Photo(
                image_filename=image_filename,
                image_url=image_url,
                user_id=current_user.id,
            )
            db.session.add(picture_photo)
            db.session.commit()
            flash("Photo saved.")

            return redirect(url_for('main.details_view'))
        else:
            flash('ERROR! Photo was not saved.', 'error')
    return render_template('main/upload.html', form=form)


#@main.route('/faq')
#def faq():
    #editable_html_obj = EditableHTML.get_editable_html('faq')
    #return render_template('main/faq.html',
                           #editable_html_obj=editable_html_obj)


#@main.route('/about')
#def about():
    #editable_html_obj = EditableHTML.get_editable_html('about')
    #return render_template('main/about.html',
                           #editable_html_obj=editable_html_obj)

@main.route('/faq')
def faq():

    #return render_template('main/faq.html')
    return redirect(url_for('main.about'))

@main.route('/about')
def about():
    return render_template('main/about.html')



@main.route('/parents')
def landing_page_parents():
    if current_user.is_anonymous:
        return render_template('landing/landing_parents.html')
    parent_details_exist = db.session.query(Parent).filter(Parent.user_id == current_user.id).count()
    if current_user.is_authenticated and parent_details_exist < 1:
        return redirect(url_for('main.create_profile_parent'))
    elif current_user.is_authenticated and parent_details_exist >= 1:
        return redirect(url_for('main.index'))
    else:
        return render_template('landing/landing_parents.html')

@main.route('/childminders/nannies/babysitters')
@main.route('/')
@main.route('/childminder/nanny/babysitter')
def landing_page_carers():
    if current_user.is_anonymous:
        return render_template('landing/landing_carers.html')
    carer_details_exist = db.session.query(Carer).filter(Carer.user_id == current_user.id).count()
    if current_user.is_authenticated and carer_details_exist < 1:

        return redirect(url_for('main.create_profile_carer'))
    elif current_user.is_authenticated and carer_details_exist == 1:
        return redirect(url_for('main.index'))
    else:
        return render_template('landing/landing_carers.html')


@main.route('/dashboard/')
@main.route('/dashboard')
@login_required
def index():
    carer = Carer.query.filter_by(user_id=current_user.id).first()
    address = Address.query.filter_by(user_id=current_user.id).first()
    availability = Availability.query.filter_by(user_id=current_user.id).first()
    photo = Photo.query.filter_by(user_id=current_user.id).first()
    parent = Parent.query.filter_by(user_id=current_user.id).first()
    workaddress = WorkAddress.query.filter_by(parent_id=current_user.id).first()
    preference = Preference.query.filter_by(parent_id=current_user.id).first()
    prefferedtime = TimePreference.query.filter_by(parent_id=current_user.id).first()
    if parent is None or carer is None:
        if address is None and availability is None and photo is None and parent is None and workaddress is None and preference is None and prefferedtime is None:
            return render_template('main/dashboard.html', user=current_user, carer = carer, address=address, availability=availability, photo=photo, parent=parent, workaddress=workaddress, preference=preference, prefferedtime=prefferedtime)
        else:
            return redirect(url_for('main.details_view'))


#####Parent Profile Starts Here ###
@main.route('/create/parent/profile', methods=['Get', 'POST'])
@login_required
def create_profile_parent():
    form = ParentForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            # filename = images.save(request.files['profile_image'])
            # url = images.url(filename)
            parent = Parent(firstname=form.firstname.data,
                            user_id=current_user.id,
                            lastname=form.lastname.data,
                            occupation=form.occupation.data,
                            mobilenumber=form.mobilenumber.data,
                            otherphonenumber=form.otherphonenumber.data,
                            nextofkinname=form.nextofkinname.data,
                            nextofkinphonenumber=form.nextofkinphonenumber.data,
                            # filename,
                            # url,
                            about_me=form.about_me.data)
            db.session.add(parent)
            db.session.commit()
            flash('Parent profile, added!', 'successful')
            address = Address.query.filter_by(user_id=current_user.id).first()
            if address is None:
                return redirect(url_for('main.add_parent_address'))

            return redirect(url_for('main.parent_detail'))
        else:

            flash('ERROR! Profile was not added.', 'error')
    return render_template('main/create_profile_parent.html', form=form)

@main.route('/address/details', methods=['Get', 'POST'])
@login_required
def address_detail():
    address = Address.query.filter_by(user_id=current_user.id).first()
    return render_template('main/address_details.html',address=address)

@main.route('/create/child/profile/add', methods=['Get', 'POST'])
@login_required
def create_profile_child():
    form = ChildForm()
    if request.method == 'POST':
        if form.validate_on_submit():

            # filename = images.save(request.files['profile_image'])
            # url = images.url(filename)
            child = Child(child_firstname=form.child_firstname.data,
                          user_id=current_user.id,
                          child_lastname=form.child_lastname.data,
                          child_age=form.child_age.data,
                          child_allergy=form.child_allergy.data,
                          child_disability=form.child_disability.data,
                          child_special_needs=form.child_special_needs.data)
            # filename,
            # url,

            db.session.add(child)
            db.session.commit()
            flash('Child information, added!', 'successful')
            timePreference = TimePreference.query.filter_by(user_id=current_user.id).first()
            if timePreference is None:
                return redirect(url_for('main.add_time_preference'))

            return redirect(url_for('main.index'))
        else:

            flash('ERROR! Data was not added.', 'error')
    return render_template('main/create_profile_child.html', form=form)


@main.route('/child/details', methods=['GET', 'POST'])
@login_required
def child_details():
    child = Child.query.filter_by(user_id=current_user.id).first()
    return render_template('main/child_details.html', child=child)

@main.route('/preferences/add', methods=['GET', 'POST'])
@login_required
def add_preferences():
    """Parent add a preference."""
    form = PreferenceForm()
    if form.validate_on_submit():
        print('passed this point')
        preference = Preference(nosofotherkids=form.nosofotherkids.data,
                                user_id=current_user.id,
                                parent_id=current_user.id,
                                nosofownkids=form.nosofownkids.data,
                                smoke=form.smoke.data,
                                rateperhour=form.rateperhour.data,
                                county=form.county.data,
                                carer_type=form.carer_type.data,
                                live_in_or_out=form.live_in_or_out.data,
                                location=form.location.data,
                                religion=form.religion.data,
                                ofstead_childminder_license_holder=form.ofstead_childminder_license_holder.data,
                                sex=form.sex.data)
        db.session.add(preference)
        db.session.commit()
        flash('form-success')
        child = Child.query.filter_by(user_id=current_user.id).first()
        if child is None:
            return redirect(url_for('main.create_profile_child'))

        return redirect(url_for('main.index'))
    else:
        flash('ERROR! Data was not added.', 'error')

    return render_template('main/add_preference.html', form=form)


@main.route('/dashboard/preference/time/add', methods=['Get', 'POST'])
@login_required
def add_time_preference():
    form = TimePreferenceForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            time_preference = TimePreference(user_id=current_user.id,
                                             parent_id=current_user.id,
                                             number_of_hours_mon=form.number_of_hours_mon.data,
                                             monday_available=form.monday_available.data,
                                             number_of_hours_tue=form.number_of_hours_tue.data,
                                             tuesday_available=form.tuesday_available.data,
                                             number_of_hours_wed=form.number_of_hours_wed.data,
                                             wednesday_available=form.wednesday_available.data,
                                             number_of_hours_thur=form.number_of_hours_thur.data,
                                             thursday_available=form.thursday_available.data,
                                             number_of_hours_fri=form.number_of_hours_fri.data,
                                             friday_available=form.friday_available.data,
                                             number_of_hours_sat=form.number_of_hours_sat.data,
                                             saturday_available=form.saturday_available.data,
                                             number_of_hours_sunday=form.number_of_hours_sunday.data,
                                             sunday_available=form.sunday_available.data)
            db.session.add(time_preference)
            db.session.commit()
            flash('Information, added!', 'successful')

            photo = Photo.query.filter_by(user_id=current_user.id).first()
            if photo is None:
                return redirect(url_for('main.upload'))
            return redirect(url_for('main.time_preference_detail'))
        else:

            flash('ERROR! Data was not added.', 'error')
    return render_template('main/add_time_preference.html', form=form)


@main.route('/parent/address/add', methods=['GET', 'POST'])
@login_required
def add_parent_address():
    """User add an address."""
    form = AddressForm()
    if form.validate_on_submit():
        address = Address(property_subdivision=form.property_subdivision.data,
                          user_id=current_user.id,
                          property_number_street_address=form.property_number_street_address.data,
                          locality_address=form.locality_address.data,
                          post_town=form.post_town.data,
                          postcode=form.postcode.data,
                          county=form.county.data)
        db.session.add(address)
        db.session.commit()
        flash('form-success')
        address = WorkAddress.query.filter_by(parent_id=current_user.id).first()
        if address is None:
            return redirect(url_for('main.add_work_address'))

        return redirect(url_for('main.index'))
    else:
        flash('ERROR! Data was not added.', 'error')

    return render_template('main/add_address.html', form=form)

@main.route('/parent/work/address', methods=['GET', 'POST'])
@login_required
def work_address_details():
    address = WorkAddress.query.filter_by(parent_id=current_user.id).first()
    return render_template('main/work_address_details.html', address=address)

@main.route('/work/address/add', methods=['GET', 'POST'])
@login_required
def add_work_address():
    """Parent add work address."""
    form = WorkAddressForm()
    if form.validate_on_submit():
        address = WorkAddress(property_subdivision=form.property_subdivision.data,
                              parent_id=current_user.id,
                              # user_id=current_user.id,
                              property_number_street_address=form.property_number_street_address.data,
                              # locality_address=form.locality_address.data,
                              post_town=form.post_town.data,
                              postcode=form.postcode.data,
                              county=form.county.data,
                              company_name=form.company_name.data)
        db.session.add(address)
        db.session.commit()
        flash('form-success')
        preference = Preference.query.filter_by(user_id=current_user.id).first()
        if preference is None:
            return redirect(url_for('main.add_preferences'))

        return redirect(url_for('main.index'))
    else:
        flash('ERROR! Data was not added.', 'error')

    return render_template('main/add_work_address.html', form=form)


@main.route('/preferences/edit', methods=['GET', 'POST'])
@login_required
def edit_preferences():
    """Parent edit added preference."""
    form = PreferenceForm()
    if form.validate_on_submit():
        preference = Preference()
        db.session.add(preference)
        db.session.commit()
        flash('form-success')
        return redirect(url_for('main.index'))
    else:

        print('Your data was not saved')
    return redirect(url_for('main.index'))


#####Carer Profile Starts Here ###
@main.route('/create/carer/profile', methods=['Get', 'POST'])
@login_required
def create_profile_carer():
    form = CarerForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            # filename = images.save(request.files['profile_image'])
            # url = images.url(filename)
            carer = Carer(carer_firstname=form.carer_firstname.data,
                          user_id=current_user.id,
                          carer_lastname=form.carer_lastname.data,
                          careexperience=form.careexperience.data,
                          nationalinsurancenumber=form.nationalinsurancenumber.data,
                          mobilephonenumber=form.mobilephonenumber.data,
                          ofsteadnumber=form.ofsteadnumber.data,
                          rateperhour=form.rateperhour.data,
                          religion=form.religion.data,
                          smoke=form.smoke.data,
                          criminalconviction=form.criminalconviction.data,
                          sex_offender=form.sex_offender.data,
                          nosofownkids=form.nosofownkids.data,
                          nosoftherkids=form.nosoftherkids.data,
                          carer_type=form.carer_type.data,
                          live_in_or_out=form.live_in_or_out.data,
                          location=form.location.data,
                          carer_age=form.carer_age.data,
                          # filename,
                          # url,
                          about_me=form.about_me.data,
                          sex=form.sex.data)
            db.session.add(carer)
            db.session.commit()
            flash('Profile, added!', 'successful')

            address = Address.query.filter_by(user_id=current_user.id).first()
            if address is None:
                return redirect(url_for('main.carer_add_address'))

            #return render_template('main/details.html', user=current_user)
            return redirect(url_for('main.carer_detail', user_id=current_user.id))
        else:
            flash('ERROR! Profile was not added.', 'error')

    return render_template('main/create_carer_profile.html', form=form)


@main.route('/address/add', methods=['GET', 'POST'])
@login_required
def carer_add_address():
    """User add an address."""
    form = AddressForm()
    if form.validate_on_submit():
        address = Address(property_subdivision=form.property_subdivision.data,
                          # user.user_type=user.user_type,
                          user_id=current_user.id,
                          carer_id=current_user.id,
                          property_number_street_address=form.property_number_street_address.data,
                          locality_address=form.locality_address.data,
                          post_town=form.post_town.data,
                          postcode=form.postcode.data,
                          county=form.county.data)
        db.session.add(address)
        db.session.commit()
        flash('form-success')
        address = Availability.query.filter_by(user_id=current_user.id).first()
        if address is None:
            return redirect(url_for('main.add_availability'))

        return redirect(url_for('main.index'))
    else:
        flash('ERROR! Data was not added.', 'error')

    return render_template('main/add_address.html', form=form)


@main.route('/dashboard/availability/add', methods=['Get', 'POST'])
@login_required
def add_availability():
    ''' Carers add their availabilities '''
    form = AvailabilityForm()
    if request.method == 'POST':
        if form.validate_on_submit():

            # if form.monday_available.data:
            #     form.monday_available.data = 1
            # else:
            #     form.monday_available.data = 0
            #
            # if form.monday_time_available_day.data:
            #     form.monday_time_available_day.data = 1
            # else:
            #     form.monday_time_available_day.data = 0
            #
            # if form.monday_time_available_night.data:
            #     form.monday_time_available_night.data = 1
            # else:
            #     form.monday_time_available_night.data = 0
            #
            # if form.tuesday_available.data:
            #     form.tuesday_available.data = 1
            # else:
            #     form.tuesday_available.data = 0
            #
            # if form.tuesday_time_available_day.data:
            #     form.tuesday_time_available_day.data = 1
            # else:
            #     form.tuesday_time_available_day.data = 0
            #
            # if form.tuesday_time_available_night.data:
            #     form.tuesday_time_available_night.data = 1
            # else:
            #     form.tuesday_time_available_night.data = 0
            #
            # if form.wednesday_available.data:
            #     form.wednesday_available.data = 1
            # else:
            #     form.wednesday_available.data = 0
            #
            # if form.wednesday_time_available_day.data:
            #     form.wednesday_time_available_day.data = 1
            # else:
            #     form.wednesday_time_available_day.data = 0
            #
            # if form.wednesday_time_available_night.data:
            #     form.wednesday_time_available_night.data = 1
            # else:
            #     form.wednesday_time_available_night.data = 0
            #
            # if form.thursday_available.data:
            #     form.thursday_available.data = 1
            # else:
            #     form.thursday_available.data = 0
            #
            # if form.thursday_time_available_day.data:
            #     form.thursday_time_available_day.data = 1
            # else:
            #     form.thursday_time_available_day.data = 0
            #
            # if form.thursday_time_available_night.data:
            #     form.thursday_time_available_night.data = 1
            # else:
            #     form.thursday_time_available_night.data = 0
            #
            # if form.friday_available.data:
            #     form.friday_available.data = 1
            # else:
            #     form.friday_available.data = 0
            #
            # if form.friday_time_available_day.data:
            #     form.friday_time_available_day.data = 1
            # else:
            #     form.friday_time_available_day.data = 0
            #
            # if form.friday_time_available_night.data:
            #     form.friday_time_available_night.data = 1
            # else:
            #     form.friday_time_available_night.data = 0
            #
            # if form.saturday_available.data:
            #     form.saturday_available.data = 1
            # else:
            #     form.saturday_available.data = 0
            #
            # if form.saturday_time_available_day.data:
            #     form.saturday_time_available_day.data = 1
            # else:
            #     form.saturday_time_available_day.data = 0
            #
            # if form.saturday_time_available_night.data:
            #     form.saturday_time_available_night.data = 1
            # else:
            #     form.saturday_time_available_night.data = 0
            #
            # if form.sunday_available.data:
            #     form.sunday_available.data = 1
            # else:
            #     form.sunday_available.data = 0
            #
            # if form.sunday_time_available_day.data:
            #     form.sunday_time_available_day.data = 1
            # else:
            #     form.sunday_time_available_day.data = 0
            #
            # if form.sunday_time_available_night.data:
            #     form.sunday_time_available_night.data = 1
            # else:
            #     form.sunday_time_available_night.data = 0

            availability = Availability(user_id=current_user.id,
                          number_of_hours_mon=form.number_of_hours_mon.data,
                          monday_available=form.monday_available.data,
                          number_of_hours_tue=form.number_of_hours_tue.data,
                          tuesday_available=form.tuesday_available.data,
                          number_of_hours_wed=form.number_of_hours_wed.data,
                          wednesday_available=form.wednesday_available.data,
                          number_of_hours_thur=form.number_of_hours_thur.data,
                          thursday_available=form.thursday_available.data,
                          number_of_hours_fri=form.number_of_hours_fri.data,
                          friday_available=form.friday_available.data,
                          number_of_hours_sat=form.number_of_hours_sat.data,
                          saturday_available=form.saturday_available.data,
                          number_of_hours_sunday=form.number_of_hours_sunday.data,
                          sunday_available=form.sunday_available.data)
            db.session.add(availability)
            db.session.commit()
            flash('Information, added!', 'successful')
            photo = Photo.query.filter_by(user_id=current_user.id).first()
            if photo is None:
                 return redirect(url_for('main.upload'))

            return redirect(url_for('main.availability_detail'))
        else:
            flash('ERROR! Data was not added.', 'error')
    return render_template('main/add_availability.html', form=form)


##### Detail Views for both parents and carers###

@main.route('/dashboard/parent/profile/')
@login_required
def parent_detail():
    """Provide HTML page with all details on a given parent """
    detail = None
    try:
        detail = Parent.query.filter_by(user_id=current_user.id).all()[0]

    except IndexError:
        pass

    if detail is not None:
        time_preference_detail = TimePreference.query.filter_by(user_id=current_user.id).first()
        preference_detail = Preference.query.filter_by(user_id=current_user.id).first()
        photo = Photo.query.filter_by(user_id=current_user.id).first()
        child = Child.query.filter_by(user_id=current_user.id).first()

        return render_template('main/parent_detail.html', detail=detail, preference_detail=preference_detail,
                               user=current_user, time_preference_detail=time_preference_detail,
                               photo=photo, child=child)


    elif detail == None:
        return redirect(url_for('main.create_profile_parent'))

    else:
        abort(404)


@main.route('/dashboard/profile/preference/detail')
@login_required
def preference_detail():
    preference_detail = None
    try:
        preference_detail = Preference.query.filter_by(user_id=current_user.id).all()[0]

    except IndexError:
        pass

    if preference_detail is not None:
        time_preference_detail = TimePreference.query.filter_by(user_id=current_user.id).all()[0]

        return render_template('main/preference_detail.html', preference_detail=preference_detail, user=current_user,
                               time_preference_detail=time_preference_detail)


    elif preference_detail == None:
        return redirect(url_for('main.add_preferences'))

    else:
        abort(404)


@main.route('/dashboard/profile/preference/time/detail')
@login_required
def time_preference_detail():
    time_preference_detail = None
    try:
        time_preference_detail = TimePreference.query.filter_by(user_id=current_user.id).all()[0]

    except IndexError:
        pass

    if time_preference_detail is not None:

        return render_template('main/time_preference_detail.html', time_preference_detail=time_preference_detail,
                               user=current_user)


    elif time_preference_detail == None:
        return redirect(url_for('main.add_time_preference'))

    else:
        abort(404)


@main.route('/carer/profile/<int:user_id>/')
@login_required
def carer_detail(user_id):
    """Provide HTML page with all details on a given parent """
    detail_carer = None
    try:
        detail_carer = Carer.query.filter_by(user_id=current_user.id).all()[0]

    except IndexError:
        pass

    if detail_carer is not None:
        availability = Availability.query.filter_by(user_id=current_user.id).all()
        photo = Photo.query.filter_by(user_id=current_user.id).first()
        address = Address.query.filter_by(user_id=current_user.id).first()

        return render_template('main/carer_detail.html', availability=availability,
                               detail_carer=detail_carer, user=current_user,
                               photo=photo, address=address)

    elif detail_carer == None:
        return redirect(url_for('main.create_profile_carer'))

    else:
        abort(404)


@main.route('/public/profile/<int:id>/')
@login_required
def public_carer_detail(id):
    detail_carer = Carer.query.get(id)
    availability = Availability.query.get(id)
    photo = Photo.query.get(id)
    address = Address.query.get(id)

    return render_template('main/public_carers.html', availability=availability,
                           detail_carer=detail_carer, user=current_user,
                           photo=photo, address=address)


@main.route('/dashboard/availability/detail')
@login_required
def availability_detail():
    availability_detail = None
    try:
        availability_detail = Availability.query.filter_by(user_id=current_user.id).all()[0]

    except IndexError:
        pass

    if availability_detail is not None:
        return render_template('main/availability_detail.html', availability_detail=availability_detail,
                               user=current_user)

    elif availability_detail == None:
        return redirect(url_for('main.add_availability'))

    else:
        abort(404)


@main.route('/details/')
@login_required
def details_view():
    return render_template('main/details.html', user=current_user)

@main.route('/booking/', methods=['Get', 'POST'])
@login_required
def add_booking_carer():
    photo = Photo.query.filter_by(user_id=current_user.id).first()
    booking_form = BookingForm()
    availability_details = None
    try:
        load_user = Carer.query.order_by().distinct(Carer.user_id)
        availability_details = Availability.query.all()
    except IndexError:
        pass
    if load_user is not None and current_user.user_type == "Parent":
        if request.method == 'POST':
            request.form
            if booking_form.validate_on_submit():
                bookings = Booking(user_id=current_user.id,
                                   monday_day_time=booking_form.monday_day_time.data,
                                   monday_night_time=booking_form.monday_night_time.data,
                                   tuesday_day_time=booking_form.tuesday_day_time.data,
                                   tuesday_night_time=booking_form.tuesday_night_time.data,
                                   wednesday_day_time=booking_form.wednesday_day_time.data,
                                   wednesday_night_time=booking_form.wednesday_night_time.data,
                                   thursday_day_time=booking_form.thursday_day_time.data,
                                   thursday_night_time=booking_form.thursday_night_time.data,
                                   friday_day_time=booking_form.friday_day_time.data,
                                   friday_night_time=booking_form.friday_night_time.data,
                                   saturday_day_time=booking_form.saturday_day_time.data,
                                   saturday_night_time=booking_form.saturday_night_time.data,
                                   sunday_day_time=booking_form.sunday_day_time.data,
                                   sunday_night_time=booking_form.sunday_night_time.data,
                                   parent_id=current_user.id)

                db.session.add(bookings)
                db.session.commit()
                flash('form-success')
                return render_template('main/booking_carer.html', form=booking_form, user=current_user)

        for v in load_user:
            v.rateperhour
        return render_template('main/booking_carer.html',load_user=load_user,  availability_details=availability_details,  form = booking_form,  user=current_user, photo_link = photo.image_url)

    else:
        return redirect(url_for('main.availability_detail'))


@main.route('/bookings/', methods=['Get', 'POST'])
@login_required
def add_bookings():
    form = PhotoForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            bookings = Bookings(user_id=current_user.id,
                                start_date=form.start_date.data,
                                end_date=form.start_date.data)
            db.session.add(bookings)
            db.session.commit()

            flash('form-success')
            return render_template('main/booking.html', form=form, carer_id=3)
        else:
            flash('ERROR! Date and time was not added.', 'error')
    return redirect(url_for('main.add_availability'))
    #return render_template('main/availability_detail.html', user=current_user)

    # render_template('course/payment.html', course=course, amount=amount,
    # key=app.config.get('STRIPE_PUBLISHABLE_KEY'))


@main.route('/booking/<int:carerid>/<float:rateperhour>/add', methods=['Get', 'POST'])
@login_required
def add_booking(carerid,rateperhour):
    global childminder_id
    childminder_id = carerid
    booking_form = BookingForm()
    if request.method == 'POST':
        count=0
        if booking_form.validate_on_submit():
            bookings = Booking(user_id=current_user.id,
                               monday_day_time=booking_form.monday_day_time.data,
                               monday_night_time=booking_form.monday_night_time.data,
                               tuesday_day_time=booking_form.tuesday_day_time.data,
                               tuesday_night_time=booking_form.tuesday_night_time.data,
                               wednesday_day_time=booking_form.wednesday_day_time.data,
                               wednesday_night_time=booking_form.wednesday_night_time.data,
                               thursday_day_time=booking_form.thursday_day_time.data,
                               thursday_night_time=booking_form.thursday_night_time.data,
                               friday_day_time=booking_form.friday_day_time.data,
                               friday_night_time=booking_form.friday_night_time.data,
                               saturday_day_time=booking_form.saturday_day_time.data,
                               saturday_night_time=booking_form.saturday_night_time.data,
                               sunday_day_time=booking_form.sunday_day_time.data,
                               sunday_night_time=booking_form.sunday_night_time.data,
                               parent_id=current_user.id,
                               carer_id=carerid)

            db.session.add(bookings)
            db.session.commit()

            carer_info = Availability.query.filter_by(carer_id=carerid)[0]

            if booking_form.monday_day_time.data:
                carer_info.monday_time_available_day = 0
                count=count+1

            if booking_form.monday_night_time.data:
                carer_info.monday_time_available_night = 0
                count +=  1

            if booking_form.tuesday_day_time.data:
                carer_info.tuesday_time_available_day = 0
                count +=  1

            if booking_form.tuesday_night_time.data:
                carer_info.tuesday_time_available_night = 0
                count +=  1

            if booking_form.wednesday_day_time.data:
                carer_info.wednesday_time_available_day = 0
                count +=  1

            if booking_form.wednesday_night_time.data:
                carer_info.wednesday_time_available_night = 0
                count +=  1

            if booking_form.thursday_day_time.data:
                carer_info.thursday_time_available_day = 0
                count +=  1

            if booking_form.thursday_night_time.data:
                carer_info.thursday_time_available_night = 0
                count +=  1

            if booking_form.friday_day_time.data:
                carer_info.friday_time_available_day = 0
                count +=  1

            if booking_form.friday_night_time.data:
                carer_info.friday_time_available_night = 0
                count +=  1

            if booking_form.saturday_day_time.data:
                carer_info.saturday_time_available_day = 0
                count +=  1

            if booking_form.saturday_night_time.data:
                carer_info.saturday_time_available_night = 0
                count +=  1

            if booking_form.sunday_day_time.data:
                carer_info.sunday_time_available_day = 0
                count +=  1

            if booking_form.sunday_night_time.data:
                carer_info.sunday_time_available_night = 0
                count +=  1

            db.session.commit()
            amount = count*rateperhour*8*100
            amount = amount+(amount/100)*7
            flash('form-success')

            return redirect(url_for('main.checkout',amount=amount))
        #    return render_template('main/thankyou.html', form=booking_form, user=current_user)

        else:

            flash('ERROR! Data was not added.', 'error')

    photo = Photo.query.filter_by(user_id=current_user.id).first()
    load_user = Carer.query.order_by().distinct(Carer.user_id)
    availability_details = Availability.query.all()
    return render_template('main/booking_carer.html',availability_details=availability_details,load_user=load_user, form=booking_form, user = current_user,photo_link = photo.image_url)

