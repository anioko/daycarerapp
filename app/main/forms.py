from flask_wtf import Form
from wtforms import BooleanField, RadioField, IntegerField, StringField, StringField, SubmitField, SelectField, DateField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, NumberRange, Length, required, Required, EqualTo, ValidationError, InputRequired, Email
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms.fields.html5 import EmailField
from flask_pagedown.fields import PageDownField
#from . import images
from flask_uploads import UploadSet, IMAGES, configure_uploads
images = UploadSet('images', IMAGES)

from .. import db
from ..models import Role, User
from wtforms_components import TimeField





##class ChooseForm():
##    user_type = SelectField(u'Choose your category?', choices=[('Carer','carer'),('Parent','parent')])
##    submit = SubmitField('Submit')
##    

#### Parents fill out these part on their profile ####

class PhotoForm(Form):
    photo = FileField('Profile Picture', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    submit = SubmitField('Submit')

class BookingForm(Form):

    """ This is the form where parents fill out their profile details  """
    
    start_date = DateField('Start Date', format="%m/%d/%Y")
    end_date = DateField('End Date', format="%m/%d/%Y")
    start_time = TimeField('Start Time', format="%m/%d/%Y")
    end_time = TimeField('End Time', format="%m/%d/%Y")
    #num_of_hours = IntegerField('Total number of hours e.g 10', validators=[DataRequired()])
    submit = SubmitField('Submit')
    
class ParentForm(Form):

    """ This is the form where parents fill out their profile details  """
    
    firstname = StringField('First Name', validators=[DataRequired()])
    lastname = StringField('Last Name', validators=[DataRequired()])
    occupation = StringField('Occupation', validators=[DataRequired()])
    mobilenumber = StringField('Mobile Phone Number', validators=[DataRequired()])
    otherphonenumber = StringField('Any Other Phone Number', validators=[DataRequired()])
    nextofkinname = StringField('Next of Kin', validators=[DataRequired()])
    nextofkinphonenumber = StringField('Next of Kin Phone Number', validators=[DataRequired()])
    #parent_image = FileField('Profile Picture', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    about_me = PageDownField('About me', validators=[], render_kw={"rows": 12, "cols": 100})
    submit = SubmitField('Submit')


    
class EditParentForm(Form):

    """ This is the form where parents fill out their profile details  """
    
    firstname = StringField('First Name', validators=[DataRequired()])
    lastname = StringField('Last Name', validators=[DataRequired()])
    occupation = StringField('Occupation', validators=[DataRequired()])
    mobilenumber = StringField('Mobile Phone Number', validators=[DataRequired()])
    otherphonenumber = StringField('Any Other Phone Number', validators=[DataRequired()])
    nextofkinname = StringField('Next of Kin', validators=[DataRequired()])
    nextofkinphonenumber = StringField('Next of Kin Phone Number', validators=[DataRequired()])
    parent_image = FileField('Profile Picture', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    about_me = PageDownField('About me', validators=[], render_kw={"rows": 12, "cols": 100})
    submit = SubmitField('Submit')

class ChildForm(Form):
    """ Parents provide details of their kids """
    child_firstname = StringField('Child First Name', validators=[DataRequired()])
    child_lastname = StringField('Child Last Name', validators=[DataRequired()])
    child_age = StringField('Age of the baby or child in months. E.g 5 or 13', validators=[DataRequired()])
    child_allergy = BooleanField('Child has allergy', default="")
    child_disability = BooleanField('Child has disability', default="")
    child_special_needs = BooleanField('Child has special needs', default="")
    child_description = PageDownField('Describe the child', validators=[], render_kw={"rows": 12, "cols": 100})
    submit = SubmitField('Submit')

class EditChildForm(Form):
    """ Parents provide details of their kids """
    child_firstname = StringField('Child First Name', validators=[DataRequired()])
    child_lastname = StringField('Child Last Name', validators=[DataRequired()])
    child_age = StringField('Age of the baby or child. E.g 0.5 or 1', validators=[DataRequired()])
    child_allergy = BooleanField('Child has allergy', default="")
    child_disability = BooleanField('Child has disability', default="")
    child_special_needs = BooleanField('Child has special needs', default="")
    child_description = PageDownField('Describe the child', validators=[], render_kw={"rows": 12, "cols": 100})
    submit = SubmitField('Submit')
    
class AddressForm(Form):
    """ Address Form"""                                
    property_subdivision = StringField('E.g Flat B')
    property_number_street_address = StringField('E.g 39 Acacia Avenue', validators=[DataRequired()])
    locality_address = StringField('E.g North End')
    post_town = StringField('E.g Silhurst', validators=[DataRequired()])
    postcode = StringField('E.g SH15 6BP', validators=[DataRequired()])
    county = StringField('E.g Loamshire', validators=[DataRequired()])
    submit = SubmitField('Submit')
    
class EditAddressForm(Form):
    """ Carer fills this out, we use it to provide Parent with options close to them"""                                
    property_subdivision = StringField('E.g Flat B')
    property_number_street_address = StringField('E.g 39 Acacia Avenue', validators=[DataRequired()])
    #locality_address = StringField('E.g North End', validators=[DataRequired()])
    post_town = StringField('E.g Silhurst', validators=[DataRequired()])
    postcode = StringField('E.g SH15 6BP', validators=[DataRequired()])
    county = StringField('E.g Loamshire', validators=[DataRequired()])
    submit = SubmitField('Submit')

class WorkAddressForm(Form):
    """ Carer fills this out, we use it to provide Parent with options close to them"""
    company_name = StringField('E.g Google Ltd', validators=[DataRequired()])
    property_subdivision = StringField('E.g Flat B')
    property_number_street_address = StringField('E.g 39 Acacia Avenue', validators=[DataRequired()])
    #locality_address = StringField('E.g North End', validators=[DataRequired()])
    post_town = StringField('E.g Silhurst', validators=[DataRequired()])
    postcode = StringField('E.g SH15 6BP', validators=[DataRequired()])
    county = StringField('E.g Loamshire', validators=[DataRequired()])
    submit = SubmitField('Submit')

class EditWorkAddressForm(Form):
    """ Carer fills this out, we use it to provide Parent with options close to them"""
    company_name = StringField('E.g Google Ltd', validators=[DataRequired()])
    property_subdivision = StringField('E.g Flat B')
    property_number_street_address = StringField('E.g 39 Acacia Avenue', validators=[DataRequired()])
    #locality_address = StringField('E.g North End', validators=[DataRequired()])
    post_town = StringField('E.g Silhurst', validators=[DataRequired()])
    postcode = StringField('E.g SH15 6BP', validators=[DataRequired()])
    county = StringField('E.g Loamshire', validators=[DataRequired()])
    submit = SubmitField('Submit')

class PreferenceForm(Form):
    
### Parent fills this out to tell us of their preference, we use these preferences to serve Parents with results that match their needs 
    sex = SelectField(u'Your Sex', choices=[('Male','Male'),('Female','Female')])
    nosofotherkids = IntegerField('Number of Other Kids e.g 2', validators=[DataRequired()])
    nosofownkids = IntegerField('Number of Own Kids e.g 2', validators=[DataRequired()])
    smoke = SelectField(u'Do you mind if the carer smokes?', choices=[('Yes','yes'),('No','no')])
    rateperhour = SelectField(u'Rate Per Hour in &pound; ', choices=[('5.60 ','5.60 '),('6.00','6.00'),
                                            ('7.00','7.00'),('8.00','8.00'),
                                            ('9.00','9.00'),('9.00','9.00'),('10.00','10.00'),('11.00','11.00'),
                                                               ('12.00','12.00'),('13.00','13.00'),('14.00','14.00'),
                                                               ('15.00','15.00'),('16.00','16.00'),('17.00','17.00'),
                                                               ('18.00','18.00'),('19.00','19.00'),('20.00','20.00')])
    carer_type = SelectField(u'Select Type of Carer', choices=[('Childminder','Childminder'),('Nanny','Nanny')])
    live_in_or_out = SelectField(u'Live in or out?', choices=[('Live in','Live in'),('Live outside','Live outside')])
    location = SelectField(u'Your premises?', choices=[('Yes','Yes'),('No','no')])
    county = StringField('County', validators=[DataRequired()])
    religion = SelectField(u'Religion', choices=[('Christainity','christianity'),('Hinduism','hinduism'),
                                            ('Atheism','atheism'),('Islam','islam'),
                                            ('Judaism','judaism'),('Sikhism','sikhism'),('Buddhism','buddhism')])
    ofstead_childminder_license_holder = SelectField(u'Do you prefer an Oftsead Childminder/Nanny License holder?', choices=[('Yes','yes'),('No','no')])
    submit = SubmitField('Submit')

    
class EditPreferenceForm(Form):
    
### Parent fills this out to tell us of their preference, we use these preferences to serve Parents with results that match their needs 
    sex = SelectField(u'Sex', choices=[('Male','Male'),('Female','Female')])
    nosofotherkids = IntegerField('Number of Other Kids e.g 2', validators=[DataRequired()])
    nosofownkids = IntegerField('Number of Own Kids e.g 2', validators=[DataRequired()])
    smoke = SelectField(u'Do you mind if the carer smokes?', choices=[('Yes','yes'),('No','no')])
    rateperhour = SelectField(u'Rate Per Hour in &pound; ', choices=[('5.60 ','5.60 '),('6.00','6.00'),
                                            ('7.00','7.00'),('8.00','8.00'),
                                            ('9.00','9.00'),('9.00','9.00'),('10.00','10.00'),('11.00','11.00'),
                                                               ('12.00','12.00'),('13.00','13.00'),('14.00','14.00'),
                                                               ('15.00','15.00'),('16.00','16.00'),('17.00','17.00'),
                                                               ('18.00','18.00'),('19.00','19.00'),('20.00','20.00')])
    carer_type = SelectField(u'Select Type of Carer', choices=[('Childminder','Childminder'),('Nanny','Nanny')])
    live_in_or_out = SelectField(u'Live in or out?', choices=[('Live in','Live in'),('Live outside','Live outside')])
    location = SelectField(u'Your premises?', choices=[('Yes','Yes'),('No','no')])
    county = StringField('County', validators=[DataRequired()])
    religion = SelectField(u'Religion', choices=[('Christainity','christianity'),('Hinduism','hinduism'),
                                            ('Atheism','atheism'),('Islam','islam'),
                                            ('Judaism','judaism'),('Sikhism','sikhism'),('Buddhism','buddhism')])
    ofstead_childminder_license_holder = SelectField(u'Do you prefer an Oftsead Childminder/Nanny License holder?', choices=[('Yes','yes'),('No','no')])
    submit = SubmitField('Submit')

class TimePreferenceForm(Form):
#### Carer fills this out so that we can match Parents with Carers based on the availability of Carers
    number_of_hours_mon = IntegerField('Hours carer is needed  e.g 0.5')
    monday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_tue = IntegerField('Hours carer is needed  e.g 1')
    tuesday_available = SelectField(u'Days', choices=[('tuesday','Tuesday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_wed = IntegerField('Hours carer is needed  e.g 2')
    wednesday_available = SelectField(u'Days', choices=[('wednesday','Wednesday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_thur = IntegerField('Hours carer is needed  e.g 3')
    thursday_available = SelectField(u'Days', choices=[('thursday','Thursday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_fri = IntegerField('Hours carer is needed  e.g 4')
    friday_available = SelectField(u'Days', choices=[('friday','Friday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_sat = IntegerField('Hours carer is needed  e.g 5')
    saturday_available = SelectField(u'Days', choices=[('saturday','Saturday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_sunday = IntegerField('Hours carer is needed  e.g 6')
    sunday_available = SelectField(u'Days', choices=[('sunday','Sunday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")

    submit = SubmitField('Submit')


class EditTimePreferenceForm(Form):
#### Carer fills this out so that we can match Parents with Carers based on the availability of Carers
    number_of_hours_mon = IntegerField('Hours carer is needed  e.g 0.5', validators=[DataRequired()])
    monday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_tue = IntegerField('Hours carer is needed  e.g 1', validators=[DataRequired()])
    tuesday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_wed = IntegerField('Hours carer is needed  e.g 2', validators=[DataRequired()])
    wednesday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_thur = IntegerField('Hours carer is needed  e.g 3', validators=[DataRequired()])
    thursday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_fri = IntegerField('Hours carer is needed  e.g 4', validators=[DataRequired()])
    friday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_sat = IntegerField('Hours carer is needed  e.g 5', validators=[DataRequired()])
    saturday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_sunday = IntegerField('Hours carer is needed  e.g 6', validators=[DataRequired()])
    sunday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")

    submit = SubmitField('Submit')

######Carers or Nannies or Childminders fill out this part #####


class CarerForm(Form):
    ###  This form is for the carers to provide relevant information before they can use the site to get jobs
    carer_firstname = StringField('First Name', validators=[DataRequired()])
    carer_lastname = StringField('Last Name', validators=[DataRequired()])
    careexperience = IntegerField('Years of experience e.g 3')
    mobilephonenumber = StringField('Mobile Phone Number', validators=[DataRequired()])
    nationalinsurancenumber = StringField('National Insurance Number')
    ofsteadnumber = StringField('Ofstead License Number')
    rateperhour = SelectField(u'Rate Per Hour in &pound; ', choices=[('5.60 ','5.60 '),('6.00','6.00'),
                                            ('7.00','7.00'),('8.00','8.00'),
                                            ('9.00','9.00'),('9.00','9.00'),('10.00','10.00'),('11.00','11.00'),
                                                               ('12.00','12.00'),('13.00','13.00'),('14.00','14.00'),
                                                               ('15.00','15.00'),('16.00','16.00'),('17.00','17.00'),
                                                               ('18.00','18.00'),('19.00','19.00'),('20.00','20.00')])
    religion = SelectField(u'Religion', choices=[('Christainity','christianity'),('Hinduism','hinduism'),
                                            ('Atheism','atheism'),('Islam','islam'),
                                            ('Judaism','judaism'),('Sikhism','sikhism'),('Buddhism','buddhism')])
    #carer_image = FileField('Profile Picture', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    #carer_living_space = FileField('Space to use in offering services', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    smoke = SelectField(u'Do you smoke', choices=[('Yes','yes'),('No','no')])
    criminalconviction = SelectField(u'Any criminal conviction?', choices=[('Yes','yes'),('No','no')])
    sex_offender = SelectField(u'Are you a sex offender?', choices=[('Yes','yes'),('No','no')])
    nosofownkids = IntegerField('Number of Your Own Kids e.g 2')
    nosoftherkids = IntegerField('Number of Other Kids under your carer e.g 2')
    carer_type = SelectField(u'Select Type of Carer', choices=[('Childminder','Childminder'),('Nanny','Nanny')])
    live_in_or_out = SelectField(u'Live in or out?', choices=[('Live in','Live in'),('Live outside','Live outside')])
    location = SelectField(u'Your premises?', choices=[('Yes','Yes'),('No','no')])
    about_me = PageDownField('About me', validators=[], render_kw={"rows": 12, "cols": 100})
    carer_age = IntegerField('Your age e.g 20', validators=[DataRequired()])
    sex = SelectField(u'Sex', choices=[('Male','Male'),('Female','Female')])
    submit = SubmitField('Submit')
                                    

class EditCarerForm(Form):
    ###  This form is for the carers to provide relevant information before they can use the site to get jobs
    carer_firstname = StringField('DayCarer First Name', validators=[DataRequired()])
    carer_lastname = StringField('DayCarer Last Name', validators=[DataRequired()])
    careexperience = IntegerField('Years of experience e.g 3', validators=[DataRequired()])
    mobilephonenumber = StringField('Mobile Phone Number', validators=[DataRequired()])
    nationalinsurancenumber = StringField('National Insurance Number', validators=[DataRequired()])
    ofsteadnumber = StringField('Ofstead', validators=[DataRequired()])
    rateperhour = SelectField(u'Rate Per Hour in &pound; ', choices=[('5.60 ','5.60 '),('6.00','6.00'),
                                            ('7.00','7.00'),('8.00','8.00'),
                                            ('9.00','9.00'),('9.00','9.00'),('10.00','10.00'),('11.00','11.00'),
                                                               ('12.00','12.00'),('13.00','13.00'),('14.00','14.00'),
                                                               ('15.00','15.00'),('16.00','16.00'),('17.00','17.00'),
                                                               ('18.00','18.00'),('19.00','19.00'),('20.00','20.00')])
    religion = SelectField(u'Religion', choices=[('Christainity','christianity'),('Hinduism','hinduism'),
                                            ('Atheism','atheism'),('Islam','islam'),
                                            ('Judaism','judaism'),('Sikhism','sikhism'),('Buddhism','buddhism')])
    #carer_image = FileField('Profile Picture', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    #carer_living_space = FileField('Space to use in offering services', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    smoke = SelectField(u'Do you smoke', choices=[('Yes','yes'),('No','no')])
    criminalconviction = SelectField(u'Any criminal conviction?', choices=[('Yes','yes'),('No','no')])
    sex_offender = SelectField(u'Are you a sex offender?', choices=[('Yes','yes'),('No','no')])
    rateperhour = IntegerField('Rate per hour e.g 10', validators=[DataRequired()])
    nosofownkids = IntegerField('Number of Your Own Kids e.g 2', validators=[DataRequired()])
    nosoftherkids = IntegerField('Number of Other Kids under your carer e.g 2', validators=[DataRequired()])
    carer_type = SelectField(u'Select Type of Carer', choices=[('Childminder','Childminder'),('Nanny','Nanny')])
    live_in_or_out = SelectField(u'Live in or out?', choices=[('Live in','Live in'),('Live outside','Live outside')])
    location = SelectField(u'Your premises?', choices=[('Yes','Yes'),('No','no')])
    about_me = PageDownField('About me', validators=[], render_kw={"rows": 12, "cols": 100})
    carer_age = IntegerField('Your age e.g 2', validators=[DataRequired()])
    sex = SelectField(u'Sex', choices=[('Male','Male'),('Female','Female')])
    submit = SubmitField('Submit')
                                    


class AvailabilityForm(Form):
#### Carer fills this out so that we can match Parents with Carers based on the availability of Carers
    number_of_hours_mon = IntegerField('Hours available  e.g 0')
    monday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_tue = IntegerField('Hours available  e.g 1')
    tuesday_available = SelectField(u'Days', choices=[('tuesday','Tuesday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_wed = IntegerField('Hours available  e.g 2')
    wednesday_available = SelectField(u'Days', choices=[('wednesday','Wednesday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_thur = IntegerField('Hours available  e.g 3')
    thursday_available = SelectField(u'Days', choices=[('thursday','Thursday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_fri = IntegerField('Hours available  e.g 4')
    friday_available = SelectField(u'Days', choices=[('friday','Friday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_sat = IntegerField('Hours available  e.g 5')
    saturday_available = SelectField(u'Days', choices=[('saturday','Saturday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_sunday = IntegerField('Hours available for  e.g 6')
    sunday_available = SelectField(u'Days', choices=[('sunday','Sunday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")

    submit = SubmitField('Submit')


class EditAvailabilityForm(Form):
#### Carer fills this out so that we can match Parents with Carers based on the availability of Carers
    number_of_hours_mon = IntegerField('Hours available  e.g 0', validators=[DataRequired()])
    monday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_tue = IntegerField('Hours available  e.g 1', validators=[DataRequired()])
    tuesday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_wed = IntegerField('Hours available  e.g 2', validators=[DataRequired()])
    wednesday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_thur = IntegerField('Hours available  e.g 3', validators=[DataRequired()])
    thursday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_fri = IntegerField('Hours available  e.g 4', validators=[DataRequired()])
    friday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_sat = IntegerField('Hours available  e.g 5', validators=[DataRequired()])
    saturday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")
    number_of_hours_sunday = IntegerField('Hours available for  e.g 6', validators=[DataRequired()])
    sunday_available = SelectField(u'Days', choices=[('monday','Monday'),('tuesday','Tuesday'),
                                            ('wednesday','Wednesday'),('thursday','Thursday'),
                                            ('friday','Friday'),('saturday','Saturday'),('sunday','Sunday')], default="monday")

    submit = SubmitField('Submit')
