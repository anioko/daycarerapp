@main.route('/carer/booking/', methods=['POST'])
@login_required
def charge(carer_id, carer_firstname):
    carer = db.session.query(Carer).get(carer_id)
    service_fee = 0.03
    rate_per_hour = carer.rateperhour
    service_cost = carer.rateperhour + service_fee
    #commission = carer.rentperhour + 0.07 (to test the business model, this is disabled.)
    amount = service_cost
    amount = amount * 100  # calculate the amount
    customer = stripe.Customer.create(
        email=current_user.email,
        card=request.form['stripeToken']
    )

    charge = stripe.Charge.create(
        customer=customer.id,
        amount=int(amount),
        currency='GBP',
        description='GBP {amount} Payment for booking {carer_firstname}'.format(amount=amount, carer_firstname=carer.carer_firstname)
    )
    if charge.to_dict()['status'] == "succeeded":
        booking = Booking(user_id=current_user.id,
                          
        course.users.append(current_user)
        db.session.add(course)
        db.session.commit()
        flash("You have <strong>successfully applied</strong> for {0}.".format(course.course_title), 'success')
    else:
        flash("Some error occured. Please try again", 'warning')
    return redirect(url_for('course_list'))
    try:
        message = Message(subject="Class signed up successfully!",
                            sender='support@teachera.co.uk',
                            reply_to='support@teachera.co.uk',
                           recipients=[current_user.email])
        body = "Hello:\t{0}\nYou have signed up to a class successfully."\
                        "\nContact the organizers or the teacher for more details\n"\
                        "\n\n"\
                        "Regards,\n"\
                        "Teachera Training Ltd"  
        message.body= body.format(current_user.name)      
        mail.send(message)
    except:
        pass
        return redirect(url_for('course_list'))
