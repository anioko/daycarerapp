from flask import current_app
from flask_login import AnonymousUserMixin, UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from werkzeug.security import check_password_hash, generate_password_hash

from .. import db, login_manager

from datetime import datetime

class Permission:
    GENERAL = 0x01
    ADMINISTER = 0xff


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    index = db.Column(db.String(64))
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    @staticmethod
    def insert_roles():
        roles = {
            'User': (Permission.GENERAL, 'main', True),
            'Administrator': (
                Permission.ADMINISTER,
                'admin',
                False  # grants all permissions
            )
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            role.index = roles[r][1]
            role.default = roles[r][2]
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return '<Role \'%s\'>' % self.name


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    confirmed = db.Column(db.Boolean, default=False)
    first_name = db.Column(db.String(64), index=True)
    last_name = db.Column(db.String(64), index=True)
    email = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    user_type = db.Column(db.String(64), index=True)
    carer = db.relationship('Carer', backref='user',
                            lazy='dynamic')
    parent = db.relationship('Parent', backref='user',
                                lazy='dynamic')
    address = db.relationship('Address', backref='user',
                                lazy='dynamic')
    photo = db.relationship('Photo', backref='user',
                                lazy='dynamic')

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['ADMIN_EMAIL']:
                self.role = Role.query.filter_by(
                    permissions=Permission.ADMINISTER).first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()

    def full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def can(self, permissions):
        return self.role is not None and \
            (self.role.permissions & permissions) == permissions

    def is_admin(self):
        return self.can(Permission.ADMINISTER)

    @property
    def password(self):
        raise AttributeError('`password` is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=604800):
        """Generate a confirmation token to email a new user."""

        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    def generate_email_change_token(self, new_email, expiration=3600):
        """Generate an email change token to email an existing user."""
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def generate_password_reset_token(self, expiration=3600):
        """
        Generate a password reset change token to email to an existing user.
        """
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def confirm_account(self, token):
        """Verify that the provided token is for this user's id."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        db.session.commit()
        return True

    def change_email(self, token):
        """Verify the new email for this user."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        db.session.commit()
        return True

    def reset_password(self, token, new_password):
        """Verify the new password for this user."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        db.session.commit()
        return True

    @staticmethod
    def generate_fake(count=100, **kwargs):
        """Generate a number of fake users for testing."""
        from sqlalchemy.exc import IntegrityError
        from random import seed, choice
        from faker import Faker

        fake = Faker()
        roles = Role.query.all()

        seed()
        for i in range(count):
            u = User(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                email=fake.email(),
                password=fake.password(),
                confirmed=True,
                role=choice(roles),
                **kwargs)
            db.session.add(u)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '<User \'%s\'>' % self.full_name()


class AnonymousUser(AnonymousUserMixin):
    def can(self, _):
        return False

    def is_admin(self):
        return False

children_under_carers = db.Table('children_under_carers',
    db.Column('child_id', db.Integer, db.ForeignKey('child.id')),
    db.Column('carer_id', db.Integer, db.ForeignKey('carer.id'))
)

children_preferences = db.Table('children_preferences',
    db.Column('child_id', db.Integer, db.ForeignKey('child.id')),
    db.Column('preference_id', db.Integer, db.ForeignKey('preference.id'))
)


class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image_filename = db.Column(db.String, default=None, nullable=True)
    image_url = db.Column(db.String, default=None, nullable=True)
    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))
    #user = db.relationship(User, backref='photo')
    
class Bookings(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start_date = db.Column(db.DateTime)
    end_date = db.Column(db.DateTime)
    start_time = db.Column(db.Integer)
    end_time = db.Column(db.Integer)
    booked = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())
    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))
    parent_id = db.Column(db.Integer, db.ForeignKey('parent.id'))
    carer_id = db.Column(db.Integer, db.ForeignKey('carer.id'))
    
    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)

class Payments(db.Model):
    __tablename__ = "payment_details"
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer, default=None, nullable=True)
    carer_id = db.Column(db.Integer, db.ForeignKey('carer.id'))
    parent_id = db.Column(db.Integer, db.ForeignKey(User.id))

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)

class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    property_subdivision = db.Column(db.String, nullable=True)
    property_number_street_address = db.Column(db.String, nullable=True)
    locality_address = db.Column(db.String, nullable=True)
    postcode = db.Column(db.String, nullable=True)
    county = db.Column(db.String, nullable=True)
    post_town = db.Column(db.String, nullable=True)
    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))
    parent_id = db.Column(db.Integer, db.ForeignKey('parent.id'))
    carer_id = db.Column(db.Integer, db.ForeignKey('carer.id'))

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)
    
class WorkAddress(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    property_subdivision = db.Column(db.String, nullable=True)
    property_number_street_address = db.Column(db.String, nullable=True)
    locality_address = db.Column(db.String, nullable=True)
    postcode = db.Column(db.String, nullable=True)
    county = db.Column(db.String, nullable=True)
    company_name = db.Column(db.String, nullable=True)
    post_town = db.Column(db.String, nullable=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('parent.id'))
    
    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)


    
class Parent(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)    
    firstname = db.Column(db.String, nullable=True)
    lastname = db.Column(db.String, nullable=True)
    occupation = db.Column(db.String, nullable=True)
    mobilenumber = db.Column(db.BIGINT, default=None, nullable=True)
    otherphonenumber = db.Column(db.BIGINT, default=None, nullable=True)
    nextofkinname = db.Column(db.String, nullable=True)
    nextofkinphonenumber = db.Column(db.BIGINT, default=None, nullable=True)
    profile_image_filename = db.Column(db.String, default=None, nullable=True)
    profile_image_url = db.Column(db.String, default=None, nullable=True)
    about_me = db.Column(db.Text, default=None, nullable=True)
    addresses = db.relationship('Address', backref='parent',
                                lazy='dynamic')
    children = db.relationship('Child', backref='parent',
                                lazy='dynamic')
    preferences = db.relationship('Preference', backref='parent',
                                lazy='dynamic')
    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))

    
    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)
    
class Child(db.Model):
    """ Parents provide details of their kids """
    id = db.Column(db.Integer, primary_key=True)
    child_firstname = db.Column(db.String, nullable=True)
    child_lastname = db.Column(db.String, nullable=True)
    child_age = db.Column(db.Integer, default=None, nullable=True)
    child_allergy = db.Column(db.String, nullable=True)
    child_disability = db.Column(db.String, nullable=True)
    child_special_needs = db.Column(db.String, nullable=True)
    child_description = db.Column(db.Text, default=None, nullable=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('parent.id'))
    preference_id = db.Column(db.Integer, db.ForeignKey('preference.id'))
    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))
    #user = db.relationship(User, backref='child')



class Preference(db.Model):
    ''' Parent provide their preferences '''
    id = db.Column(db.Integer, primary_key=True)
    nosofotherkids = db.Column(db.Integer, default=None, nullable=True)
    nosofownkids = db.Column(db.Integer, default=None, nullable=True)
    smoke = db.Column(db.String, default=None, nullable=True)
    rateperhour = db.Column(db.REAL, default=None, nullable=True)
    county = db.Column(db.String, default=None, nullable=True)
    location = db.Column(db.String, default=None, nullable=True)
    live_in_or_out = db.Column(db.String, default=None, nullable=True)
    carer_type = db.Column(db.String, default=None, nullable=True)
    ofstead_childminder_license_holder = db.Column(db.String, default=None, nullable=True)
    religion = db.Column(db.String, default=None, nullable=True)
    sex = db.Column(db.String, default=None, nullable=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('parent.id'))
    children_preferences = db.relationship('Child', secondary=children_preferences,
        backref=db.backref('preferences', lazy='dynamic'))
    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))
    #user = db.relationship(User, backref='preferences')

class TimePreference(db.Model):
    __tablename__ = "preferred_times"
    id = db.Column(db.Integer, primary_key=True)
    number_of_hours_mon = db.Column(db.Integer, default=None, nullable=True)
    monday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_tue = db.Column(db.Integer, default=None, nullable=True)
    tuesday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_wed = db.Column(db.Integer, default=None, nullable=True)
    wednesday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_thur = db.Column(db.Integer, default=None, nullable=True)
    thursday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_fri = db.Column(db.Integer, default=None, nullable=True)
    friday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_sat = db.Column(db.Integer, default=None, nullable=True)
    saturday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_sunday = db.Column(db.Integer, default=None, nullable=True)
    sunday_available = db.Column(db.String, default=None, nullable=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('parent.id'))
    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))
    #user = db.relationship(User, backref='preferred_times')
    
    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)

    
######Carers or Nannies or Childminders Models#####


class Carer(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    carer_firstname = db.Column(db.String, nullable=True)
    carer_lastname = db.Column(db.String, nullable=True)
    careexperience = db.Column(db.Integer, nullable=True)
    mobilephonenumber = db.Column(db.BIGINT, default=None, nullable=True)
    nationalinsurancenumber = db.Column(db.BIGINT, nullable=True)
    ofsteadnumber = db.Column(db.BIGINT, nullable=True)
    rateperhour = db.Column(db.REAL, nullable=True)
    religion = db.Column(db.String, nullable=True)
    #profile_image_filename = db.Column(db.String, default=None, nullable=True)
    #profile_image_url = db.Column(db.String, default=None, nullable=True)
    #house_image_filename = db.Column(db.String, default=None, nullable=True)
    #house_image_url = db.Column(db.String, default=None, nullable=True)
    #carer_living_space = db.Column(db.String, default=None, nullable=True)
    smoke = db.Column(db.String, default=None, nullable=True)
    criminalconviction = db.Column(db.String, default=None, nullable=True)
    sex_offender = db.Column(db.String, default=None, nullable=True)
    nosofownkids = db.Column(db.Integer, default=None, nullable=True)
    nosoftherkids = db.Column(db.Integer, default=None, nullable=True)
    location = db.Column(db.String, default=None, nullable=True)
    live_in_or_out = db.Column(db.String, default=None, nullable=True)
    carer_type = db.Column(db.String, default=None, nullable=True)
    about_me = db.Column(db.Text, default=None, nullable=True)
    carer_age = db.Column(db.Integer, default=None, nullable=True)
    sex = db.Column(db.String, default=None, nullable=True)

    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))
    addresses = db.relationship('Address', backref='carer',
                                lazy='dynamic')

    bookings = db.relationship('Bookings', backref='carer',
                                lazy='dynamic')

    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)
    

class Availability(db.Model):
    __tablename__ = "availabilities"
    id = db.Column(db.Integer, primary_key=True)
    number_of_hours_mon = db.Column(db.Integer, default=None, nullable=True)
    monday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_tue = db.Column(db.Integer, default=None, nullable=True)
    tuesday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_wed = db.Column(db.Integer, default=None, nullable=True)
    wednesday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_thur = db.Column(db.Integer, default=None, nullable=True)
    thursday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_fri = db.Column(db.Integer, default=None, nullable=True)
    friday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_sat = db.Column(db.Integer, default=None, nullable=True)
    saturday_available = db.Column(db.String, default=None, nullable=True)
    number_of_hours_sunday = db.Column(db.Integer, default=None, nullable=True)
    sunday_available = db.Column(db.String, default=None, nullable=True)
    carer_id = db.Column(db.Integer, db.ForeignKey('carer.id'))
    user_id = db.Column(db.Integer(), db.ForeignKey(User.id))
    #user = db.relationship(User, backref='availabilities')
    
    def __repr__(self):
        return u'<{self.__class__.__name__}: {self.id}>'.format(self=self)

login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
